
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

/*
Edita.jsx
Component per crear un nou element o editar un element existent.
Si es reb el paràmetre "ID" en la ruta, es demana el registre a la base de dades i es podrà modificar
Si no arriba cap paràmetre, aleshores es considera un nou registre
El parèmtre arriba via props.match.params.id (ruta:  path="/usuaris/edita/:id")

*/
class Edita extends Component {
    constructor(props) {
        super(props);

        //si no rebem paràmetre considerem item nou: id=0
        let id = 0;
        if (this.props.match && this.props.match.params.id) {
            id = this.props.match.params.id * 1;
        }
        /*
        per defecte creem un model nou del tipus que rebem via props.
        ex) si hem rebut via props model={Usuari}
        fem un new Usuari() i l'assignem directament a this.state amb: this.state = new this.props.model();
        
        Seria l'equivalent a fer:
        let usuari = new Usuari();
        this.state = {
            nom: usuari.nom,
            email: usuari.email,
            telefon: usuari.telefon,
        }

        amb l'avantatge que no cal saber les propietats actuals de Usuari!!!

        */
        this.state = new this.props.model();
        
        //afegim altres propietats a state
        this.state.tornar=false;
    
        //bindem mètodes
        this.carregaRegistre = this.carregaRegistre.bind(this);
        this.canviInput = this.canviInput.bind(this);
        this.submit = this.submit.bind(this);

        // si item no és 0, anem a buscar-lo a la base de dades
        if (id>0){
            this.carregaRegistre(id);
        }
    }

    //carreguem registre a editar
    //i assignem les dades al state, sobreescrivint les buides
    carregaRegistre(id) {
        //getById és una promise, la crida a la bdd es asíncrona
        this.props.model.getById(id)
            .then(data => this.setState(data))
            .catch(err => console.log(err));
    }

    //cada vegada que es modifica un input es crida aquest mètode per 
    //sincronitzar el nou valor amb el State corresponent
    canviInput(event) {
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    // submit del formulari
    submit(e) {
        //evitem comportament "normal" del formulari
        e.preventDefault();
        //creem objecte model amb les dades de state
        //el constructor només prendra les que corresponguin als fields
        // si this.props.model és Usuari seria l'equivalent de fer: new Usuari(thi.state)
        let ob = new this.props.model(this.state);

        //desem el nou objecte a la BDD i actualitzem state si tot ha anat bé
        //el mètode save funciona igual tant si és un objecte nou com si ja existeix a la bdd, la diferència serà si té o no un ID>0
        ob.save()
            .then(() => this.setState({ tornar: true }))
            .catch(err => {
                console.log(err);
                this.setState({ tornar: true }); //això farà que es redirigeixi la ruta a la llista del model actual, ej: /usuaris
            });
    }


    render() {

        //si tornar és true tornem a la llista
        if (this.state.tornar) {
            return <Redirect to={'/' + this.props.model.getCollection()} />
        }

        // obtenim els camps input del model
        let inputs = this.props.model.getInputs(this.state, this.canviInput);

        // al jsx retornat hi ha un form vinculat al mètode this.submit
        // mostrant els inputs assignats en el pas anterior
        return (
            <>
                <h1 className="titol-vista">{"Edita " + this.props.model.getModelName()}</h1>
                <Form onSubmit={this.submit}>
                    <Row>
                        {inputs}
                    </Row>
                    <Row>
                        <Col>
                            <Button type="submit" color="primary">Desar</Button>
                        </Col>
                    </Row>
                </Form>
            </>
        );
    }
}






export default Edita;
