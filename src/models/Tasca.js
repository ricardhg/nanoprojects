// la constant SERVER la importem de Config.js
// és la URL de la API, així la tenim només en un lloc
import { SERVER } from '../Config';
import Usuari from './Usuari';

const MODEL = 'Tasca';
const COLLECTION = "tasques";
const ID_FIELD = 'id';
const FIELDS = [
    { name: 'descripcio', default: '', type: 'text' },
    { name: 'minuts_previstos', default: 0, type: 'number' },
 //   { name: 'id_usuari', default: null, type: 'key', relatedModel: Usuari}
];



// classe
// tot el contingut està parametritzat segons constants anteriors
// es pot copiar/enganxar en un altre modelo
// MILLORABLE: crear una classe principal i extendre-la
export default class Tasca {
 
    // el constructor pot rebre un objecte amb dades, si les rep incialitzarà els camps presents a FIELDS
    // amb els valors rebuts, si no rep res els assignarà a "default"
    constructor(data = {}) {
        // si data.id existeix l'assignem, si no assignem 0 com a ID
        this[ID_FIELD] = (data[ID_FIELD] !== undefined && data[ID_FIELD] !== null) 
                            ? data[ID_FIELD] 
                            : 0;
        //fem el mateix amb la resta de camps recorrent array FIELDS
        FIELDS.forEach(el => {
            this[el.name] = (data[el.name] !== undefined && data[el.name] !== null) ? data[el.name] : el.default
        });
    }

    //retornem titol
    static getTitle() {
        return COLLECTION;
    }
    //retornem camps
    static getFields() {
        return FIELDS;
    }
    //retornem nom del model
    static getModelName() {
        return MODEL;
    }
    //retornem nom taula
    static getCollection() {
        return COLLECTION;
    }

    // getAll demana tots els registres a la API
    // retornem una promise basada en un fetch a la API
    // si el fetch és OK tornem les dades rebudes
    // si fetch és error retornem l'error rebut pel fetch
    static getAll = () => {
        const fetchURL = `${SERVER}/${COLLECTION}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL)
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject({ error: err }) );
        });
    };

    // getById demana un registre a la API
    // el mateix que abans
    // la API Xmysql retorna un arrai amb un sol element
    // pero volem que getById retorni un objecte, per això fem data[0]
    static getById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL)
                .then(results => results.json())
                .then(data => resolve(data[0]))
                .catch(err => reject([{ error: err }]));
        });
    };


    //deleteById elimina un registre a través de la API
    static deleteById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL, { method: 'DELETE' })
                .then(resp => resp.json())
                .then(resp => {
                    resolve(resp);
                })
                .catch(err => reject([{ error: err }]) );
        });
    };


    /* getInputs torna els camps input per al form Edita 
    els inputs es creen a partir de FIELDS fent un MAP i basant-nos en el nom i el type
    important: com que és un bucle, cada Col té una key, en aquest cas utilitzem
    el mateix nom del camp, tots han de ser diferents
    per defecte fem que tots els inputs ocupin 6 moduls bootstrap, podrien ser més o menys...
    IMPORTANT: rebem state i canviInput des de Edita, per poder vincular el camp input amb la vista
    */
   static getInputs = (state, canviInput) => {
    return FIELDS.map(item => (
        <Col xs="6" key={item.name} >
            <FormGroup >
                <Label for={item.name + "Input"}>{item.name}</Label>
                <Input type={item.type}
                    name={item.name}
                    id={item.name + "Input"}
                    value={state[item.name]}
                    onChange={canviInput} />
            </FormGroup>
    </Col> ));
    }



    // Mètode NO estàtic, desa objecte actual a BDD
    // si this.id és zero, es tracta d'un nou registre >>> POST
    // si this.id és >0 es tracta d'un registre existent >>> PUT
    // en els dos casos fem un fetch a la BDD enviant
    // l'objecte this convertit a JSON
    save = () => {
        return new Promise((resolve, reject) => {
            
            // creem un nou objecte a partir de THIS
            // però sense els mètodes, només els atributs
            // l'anomenem "thisData"
            let thisData = JSON.parse(JSON.stringify(this));
            
            //si thisData.id==0 l'eliminem doncs no ens interessa que s'envii a la API
            //si no existeix fer delete no genera error
            if (!thisData[ID_FIELD]) {
                delete thisData[ID_FIELD];
            }

            //mirem si hi ha dades!
            //inicialitzem hiHaDades a false i recorrem objecte thisData mirant
            //si alguna propietat té dades (és true)
            let hiHaDades=false;
            for (var property in thisData) {
                if (thisData[property]){
                    hiHaDades=true;
                    break;
                }
            }
            //si no n'hi ha tornem error
            if (!hiHaDades){
                reject({ error: "nodata" });
                return;
            }
          
            //hi ha dades, convertim thisData en JSON i fem el fetch
            let jsonData = JSON.stringify(thisData);

            //decidim mètode per API, PUT modificarà, POST farà nou
            let method = (thisData[ID_FIELD]) ? 'PUT' : 'POST';
            //fem connexió passant dades via JSON
            fetch(`${SERVER}/${COLLECTION}`, {
                method,
                headers: new Headers({ 'Content-Type': 'application/json' }),
                    body: jsonData
                })
                .then(resp => resp.json())
                .then((resp) => resolve(resp))
                .catch(err => reject(err));

        });

    }


}